#!/usr/bin/env python
"""List and loops"""

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

our_list = [
    [1],
    [2],
    [3]
]

onehunderd_percent_not_empty_list = [i for i in range(100, 116)]
print(onehunderd_percent_not_empty_list)

float_list = [3.21, 32.12, 33.21, 65.23, 11.11]

for i in float_list:
    print(i)
