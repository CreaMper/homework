#!/usr/bin/env python

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

age = int(22)
height = float(2.29)
name = str('Arkadiusz Wieruchowski')

print(age, height, name)
