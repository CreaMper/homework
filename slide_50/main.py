#!/usr/bin/env python
"""Inheritances """

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'


class Keyboard:
    """Keyboard class"""
    def __init__(self):
        self.price = 4500

    def show_keyboard_name(self):
        print("BlueStacker")


class Mechanical(Keyboard):
    """Class that will hold method from a parent class"""
    def __init__(self, price: int):
        self.show_keyboard_name()


class WithRedButtons(Mechanical):

    def show_keyboard_name(self):
        """Method showing us both names"""
        print("GreenMazer")
        super(WithRedButtons, self).show_keyboard_name()

