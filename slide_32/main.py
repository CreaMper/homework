#!/usr/bin/env python
"""Inheritances """

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'


class Keyboard: pass


class Mechanical(Keyboard): pass


class WithRedButtons(Mechanical): pass
