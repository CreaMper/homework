#!/usr/bin/env python
"""Cube capacity"""

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'


class Cube:
    def __init__(self, side_length: int):
        self.capacity: int = side_length ** 3

    def __eq__(self, other):
        """ Checkin' if it's equal"""
        return self.capacity == other.capacity

    def __lt__(self, other):
        """ Checkin' if it's less than"""
        return self.capacity < other.capacity

    def __gt__(self, other):
        """ Checkin' if it's greater than"""
        return self.capacity > other.capacity

    def __le__(self, other):
        """ Checkin' if it's less-equal"""
        return self.capacity <= other.capacity

    def __ge__(self, other):
        """ Checkin' if it's greater-equal"""
        return self.capacity >= other.capacity

    def __ne__(self, other):
        """ Checkin' if it's not equal"""
        return self.capacity != other.capacity


a = Cube(15)
b = Cube(15)

print(format(a == b))
