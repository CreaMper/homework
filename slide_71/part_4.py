#!/usr/bin/env python
"""Extended objects comparsion"""

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'


class Schocolate:

    def __init__(self, weight: int, amount: int):
        print("Schocolate has started")
        self.weight: int = weight
        self.amount: int = amount

    def __eq__(self, other):
        """ Checkin' if it's equal"""
        return self.weight == other.weight and self.amount == other.amount

    def __lt__(self, other):
        """ Checkin' if it's less"""
        return self.weight < other.weight and self.amount < other.amount

    def __gt__(self, other):
        """ Checkin' if it's greater than"""
        return self.weight > other.weight and self.amount > other.amount

    def __le__(self, other):
        """ Checkin' if it's less-equal"""
        return self.weight <= other.weight and self.amount <= other.amount

    def __ge__(self, other):
        """ Checkin' if it's greater-equal"""
        return self.weight >= other.weight and self.amount >= other.amount

    def __ne__(self, other):
        """ Checkin' if it's not equal"""
        return self.weight != other.weight and self.amount != other.amount


class White(Schocolate):
    def __init__(self, weight: int, amount: int):
        print("Schocolate White has been init")
        super().__init__(weight, amount)


class Black(Schocolate):
    def __init__(self, weight: int, amount: int):
        print("Black Schocolate has been init")
        super().__init__(weight, amount)


class WithNuts(White):
    def __init__(self, weight: int, amount: int):
        print("Schocolate White with nuts stuff has been init")
        super().__init__(weight, amount)


class WithPepper(Black):
    def __init__(self, weight: int, amount: int):
        print("Black Schocolate with pepper has been init")
        super().__init__(weight, amount)


class WithNuggat(Black):
    def __init__(self, weight: int, amount: int):
        print("Black Schocolate with nuggat has been init")
        super().__init__(weight, amount)


Balcerzak = WithNuggat(15, 3)
Tarczynski = Black(15, 3)

print(format(Balcerzak == Tarczynski))
