#!/usr/bin/env python
"""Inputs explained """

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'

name = input("Gimme ur name: ")
surname = input("Gimme ur surname: ")
age = input("Gimme ur age: ")

print(name, int(age), age)


def load_number():
    """ Function that will convert string given by user to integer"""
    entered_number = input("Enter a random number (str -> int): ")
    print(int(entered_number))


load_number()


class Rose:
    def __init__(self, colour: str, height: str):
        print("User said , that color of rose is ", colour, "and it's estimated height is ", height)


color = input("Rose color?: ")
height = input("Rose estimated height? : ")

rose = Rose(color, height)
